(function ($) {
  Drupal.behaviors.field_collection_datatables = {
    attach: function (context, settings) {
      var loading = '<div class="bg-warning text-center">' + Drupal.t('Loading...') + '</div>';
      $('.add-form-datatables-ajax').click(function () {
        var data_field = $(this).data('field');
        $("#ajax-" + data_field).html(loading);
        $("#ajax-" + data_field).load("/field-collection/ajax-create/" + $(this).data('type') + '/' + data_field + '/' + $(this).data('id'), function (response, status, xhr) {
          Drupal.attachBehaviors();
        });
        $("html, body").delay(1000).animate({
          scrollTop: $("#ajax-" + data_field).offset().top
        }, 1000);
      });
      $(".field-collection-datatables-view").on("click", ".edit-form-datatables-ajax", function () {
        var data_field = $(this).data('field');
        $("#ajax-" + data_field).html(loading);
        $("#ajax-" + data_field).load("/field-collection/ajax-edit/" + data_field + '/' + $(this).data('id'), function (response, status, xhr) {
          Drupal.attachBehaviors();
        });
        $("html, body").delay(1000).animate({
          scrollTop: $("#ajax-" + data_field).offset().top
        }, 1000);
      });
      $(".field-collection-datatables-view").on("click", ".duplicate-form-datatables-ajax", function () {
        var data_field = $(this).data('field');
        $("#ajax-" + data_field).html(loading);
        $("#ajax-" + data_field).load("/field-collection/ajax-duplicate/" + data_field + '/' + $(this).data('entity_type') + '/' + $(this).data('entity_id') + '/' + $(this).data('id'), function (response, status, xhr) {
          Drupal.attachBehaviors();
        });
      });
      $(".field-collection-datatables-view").on("click", ".delete-form-datatables-ajax", function () {
        if (confirm(Drupal.t("Are you sure you want to delete"))) {
          var data_field = $(this).data('field');
          $(this).closest('tr').remove();
          $("#ajax-" + data_field).load("/field-collection/ajax-delete/" + data_field + '/' + $(this).data('id'), function (response, status, xhr) {
            Drupal.attachBehaviors();
          });
          $("html, body").delay(1000).animate({
            scrollTop: $("#ajax-" + data_field).offset().top
          }, 1000);
        }
        else {
          return false;
        }
      });

      //fixed width datatables in tab bootstrap
      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($(this).attr('href') + ' table.dataTable').css('width', '');
      });
      //load ajax view
      $('.field-collection-ajax-datatables-views').each(function (index) {
        var url = "/field-collection/ajax-views/" + $(this).data('views') + '/' + $(this).data('display_id') + '?entity_id=' + $(this).data('entity_id') + '&item_id=' + $(this).data('item_id');
        $("#ajax-"+data_field ).html(loading);
        $(this).load(url, function (response, status, xhr) {
          //Drupal.attachBehaviors();
        });
      });
    }
  };
})(jQuery);
