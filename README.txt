This module look like Field Collection Table and Field Collection Views
But there is more features
- Support Datatables 1.10.16
- Support ajax for datatables and collection views
- Support field group, it looks like field group table but it shows data in mode horizontal and support datatables for sub field multi values
- Support field permission
- Support mode table for field collection (create or edit) if you wanna hide field collection when creating or editing hostEntity of field collection (In fields setting check Render with datatables)